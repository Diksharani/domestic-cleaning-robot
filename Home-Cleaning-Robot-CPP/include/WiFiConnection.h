
#ifndef wificonnection_h
#define wificonnection_h

#include <MQTTClient.h>
#include <iostream>

void callback(char* topic, byte* payload, unsigned int length);

class mywificlass {
  public:
    mywificlass();

    void stayConnected();
    void connectWiFi();                            /* Initialize and connect to AWS */
    void publishMessage(int16_t sensorValue);     /* Publish the values of the sensors */
    bool getLowBattery();


};

extern mywificlass wifiobject;

#endif