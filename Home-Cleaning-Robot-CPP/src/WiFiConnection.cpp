#include "secrets.h"
#include "WiFiConnection.h"
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <iostream>
#include <sstream>

#include "WiFi.h"
#include <HTTPClient.h>

//Your IP address or domain name with URL path


const char* TOPIC_OUT = "dataFromRover/sensorData";
const char* TOPIC_IN = "dataFromUser/lowBattery";
const char* broker = "broker.emqx.io";

WiFiClient espClient;
PubSubClient client(espClient);
bool LowBatteryState = false;


mywificlass::mywificlass() {

}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Received messages: ");
  Serial.println(topic);

  String msgReceived = "";

  for(int i = 0; i < length; i++)
  {
    Serial.print((char) payload[i]);
    msgReceived += (char) payload[i];
    
  }
  Serial.println();

  if(msgReceived == "LowBatteryTrue") LowBatteryState = true;
  else if(msgReceived == "LowBatteryFalse") LowBatteryState = false;
}

void mywificlass::connectWiFi() {

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Connecting to Wi-Fi");

  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print("Connecting...!");
  }

  Serial.println("Connected to WiFi!");

  client.setServer(broker, 1883);
  client.setCallback(callback);

}

void mywificlass::stayConnected() {
  while (!client.connected()) {
    Serial.print(".");
    delay(100);
    if(client.connect("koikoikoi"))
    {
      Serial.println("Connected to Broker!");
      client.subscribe(TOPIC_IN);
    } 
    else 
    {
      Serial.println("Trying to connect again");
      delay(5000);

    }
  }

  client.loop();
}


void mywificlass::publishMessage(int16_t sensorValue) {

  StaticJsonDocument<200> doc;
  //doc["time"] = millis();
  doc["sensor"] = sensorValue;
  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer); /* print to client */

  client.publish(TOPIC_OUT, jsonBuffer);
}

bool mywificlass::getLowBattery()
{
  return LowBatteryState;
}



mywificlass wifiobject = mywificlass();