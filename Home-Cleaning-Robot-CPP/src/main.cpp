/****************************************************
 * Created By : Anjali Singh, Jesus Lorenzo Lucas
 * Date of Creation : 12.02.2021
 ****************************************************/


#include <iostream>
#include <vector>
#include <stdio.h>
#include <string>
#include <cmath>

#include <Arduino.h>
#include <motorDriver.h>
#include <sensorDriver.h>
#include <WiFiConnection.h>

/********************** Global Function Declaration *******************************/

void taskWIFI( void * parameter);
void taskMotorMovement( void * parameter);
void taskRoverBehavior( void * parameter);
void taskObstacleDetection( void * parameter);

/*******************************************************************************/

/********************** Global Variables *****************************************/
#define LED_BOARD 2
#define CORE_0    0
#define CORE_1    1
static std::vector<int> roverCoordArray = {0, 0, 0}, targetCoordArray = {0, 0};
static boolean avoidObstacle = false;
static int16_t arraySensor[3];

hw_timer_t* timer;
/*******************************************************************************/

/*****************************************************************************
 * @name: void setup(): To initialize the peripherals and establish connection
 * @param: void
 * @return : void
****************************************************************************/
void setup(){

  Serial.begin(9600);
  pinMode(LED_BOARD, OUTPUT);
  timer = timerBegin(0,80,true);

  
  wifiobject.connectWiFi();
 
  motorobject.SETUP();
  delay(1000);
  
  sensorobject.SETUP();
  delay(1000);

  
  xTaskCreatePinnedToCore(
                    taskWIFI,          /* Task function. */
                    "taskWIFI",        /* String with name of task. */
                    8192,              /* Stack size in bytes. */
                    NULL,             /* Parameter passed as input of the task */
                    3,                /* Priority of the task. */
                    NULL,               /* Task handle. */
                    CORE_0);            /*CORE_0*/
                     /* Task handle. */

  xTaskCreatePinnedToCore(
                    taskRoverBehavior,          /* Task function. */ 
                    "taskRoverBehavior",        /* String with name of task. */
                    8192,                       /* Stack size in bytes. */
                    NULL,                       /* Parameter passed as input of the task */
                    2,                          /* Priority of the task. */
                    NULL,                       /* Task handle. */
                    CORE_0);                      /*CORE_0*/

  xTaskCreatePinnedToCore(
                    taskObstacleDetection,          /* Task function. */
                    "taskObstacleDetection",        /* String with name of task. */
                    8192,              /* Stack size in bytes. */
                    NULL,             /* Parameter passed as input of the task */
                    1,                /* Priority of the task. */
                    NULL,             /* Task handle. */
                    CORE_1);           /*CORE_0*/

}

void loop(){
    delay(1000);
}

/*************************************************************************************
 * @name: void taskWIFI( void * parameter ) : To connect to WiFi
 * @param: void
 * @return  : void
*************************************************************************************/

void taskWIFI( void * parameter )
{    
    for(;;)
    {
      /* stay connected will insure the WiFi communication */
      vTaskDelay(100/portTICK_PERIOD_MS);
      wifiobject.stayConnected();

    }   
}

/**************************************************************************************
 * @name : void taskRoverBehavior( void * parameter): To control the behaviour of rover
 * @param: void* parameter
 * @return : void
**************************************************************************************/ 

void taskRoverBehavior( void * parameter)
{
    for(;;)
    {
        if (wifiobject.getLowBattery())
        {
            while (wifiobject.getLowBattery()){
                digitalWrite(LED_BOARD, HIGH); 
                vTaskDelay(250/portTICK_PERIOD_MS);
                digitalWrite(LED_BOARD, LOW);
                vTaskDelay(250/portTICK_PERIOD_MS);
            }   
        }
                motorobject.goStraightRover(250);
                vTaskDelay(250/portTICK_PERIOD_MS);

                motorobject.stopRover();
                vTaskDelay(100/portTICK_PERIOD_MS);

        
        if ( (arraySensor[0] < 90) || (arraySensor[1] < 90) || (arraySensor[2] < 90) ) 
        {
            avoidObstacle = true;
            while(avoidObstacle) 
            {
            vTaskDelay(100/portTICK_PERIOD_MS);
            }
           
        }
    }
}

/**************************************************************************************
 * @name void taskObstacleDetection( void * parameter ) : Gets sensor values every 100 ms and manage obstacle protocol in case one is detected
 * @param: void* parameter
 * @return : void
**************************************************************************************/ 

void taskObstacleDetection( void * parameter )
{    
    for(;;)
    {
        /* Distance sensor values are stored in an array */
        arraySensor[0] = sensorobject.reading()[0];
        arraySensor[1] = sensorobject.reading()[1];
        arraySensor[2] = sensorobject.reading()[2];

        wifiobject.publishMessage(arraySensor[1]);

        vTaskDelay(100 / portTICK_PERIOD_MS);

        /* If an obstacle is detected, this task will start the obstacle protocol */

        if (avoidObstacle)
        {
            /* Depending on what sensor detects the obstacle, the rover turns clockwise or anticlockwise */
            if (arraySensor[1] < 75)
            {
           
                /* The rover moves backwards for a short time in order to avoid better the obstacle*/
                motorobject.stopRover();
                vTaskDelay(200/portTICK_PERIOD_MS);

                motorobject.goBackwardsRover(250);
                vTaskDelay(200/portTICK_PERIOD_MS);
                
                /* The rover starts turning until it stops detecting the obstacle*/

                while( (arraySensor[0] < 125) || (arraySensor[1] < 125) || (arraySensor[2] < 125) )
                {
                   motorobject.TurnClockwiseRover(250);
                   
                   arraySensor[0] = sensorobject.reading()[0];
                   arraySensor[1] = sensorobject.reading()[1];
                   arraySensor[2] = sensorobject.reading()[2];
                   vTaskDelay(100 / portTICK_PERIOD_MS);
                }
                motorobject.stopRover();                
                vTaskDelay(100/portTICK_PERIOD_MS);
                /* After non detecting the obstacle, the rover goes straight for 2 seconds */
                motorobject.goStraightRover(250);
               vTaskDelay(1000/portTICK_PERIOD_MS);
                motorobject.stopRover();
            }
            else if (arraySensor[0] < 100)
            {   

                /* The rover moves backwards for a short time in order to avoid better the obstacle*/
                motorobject.stopRover();
                vTaskDelay(200/portTICK_PERIOD_MS);
                motorobject.goBackwardsRover(250);
                vTaskDelay(200/portTICK_PERIOD_MS);

                /* The rover starts turning clockwise until it stops detecting the obstacle*/
                while( (arraySensor[0] < 125) || (arraySensor[1] < 125) || (arraySensor[2] < 125) ) 
                {
                    motorobject.TurnClockwiseRover(250);

                    arraySensor[0] = sensorobject.reading()[0];
                    arraySensor[1] = sensorobject.reading()[1];
                    arraySensor[2] = sensorobject.reading()[2];
                    vTaskDelay(100 / portTICK_PERIOD_MS);

                }
                motorobject.stopRover();
                vTaskDelay(100/portTICK_PERIOD_MS);

                /* After non detecting the obstacle, the rover goes straight for 2 seconds */
                motorobject.goStraightRover(250);
                vTaskDelay(1000/portTICK_PERIOD_MS);
                motorobject.stopRover();
            }

            else if (arraySensor[2] < 100) 
            {
                /* The rover moves backwards for a short time in order to avoid better the obstacle*/
                motorobject.stopRover();
                vTaskDelay(200/portTICK_PERIOD_MS);
                motorobject.goBackwardsRover(250);
                vTaskDelay(200/portTICK_PERIOD_MS);

                /* The rover starts turning anticlockwise until it stops detecting the obstacle*/
                while( (arraySensor[0] < 125) || (arraySensor[1] < 125) || (arraySensor[2] < 125) ) 
                {
                    motorobject.TurnAntiClockwiseRover(250);

                    arraySensor[0] = sensorobject.reading()[0];
                    arraySensor[1] = sensorobject.reading()[1];
                    arraySensor[2] = sensorobject.reading()[2];

                    vTaskDelay(100 / portTICK_PERIOD_MS);

                }
                motorobject.stopRover();

                vTaskDelay(100/portTICK_PERIOD_MS);
                /* After non detecting the obstacle, the rover goes straight for 2 seconds */
                motorobject.goStraightRover(250);
                vTaskDelay(1000/portTICK_PERIOD_MS);
                motorobject.stopRover();                
            }
            /* The variable avoidObstacle is setted to false to continue with taskRoverBehavior */
            avoidObstacle = false;
        }
    }   
}
