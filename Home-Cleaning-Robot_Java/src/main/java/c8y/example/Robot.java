
package c8y.example;

/**
 * Super Class of Home CLeaning method for general functionalities of robot.
 *
 * @author Diksha Rani, Simon Welling, Anjali Singh, Jesus Lorenzo Lucas
 * @version 02/08/2021
 */
public class Robot 
{	
    private String username;
    private double batteryLevel;
    public final double BATTERY_LEVEL_TO_START_CLEANING = 90.0;
    public final double  BATTERY_LEVEL_TO_RECHARGE = 10.0;
	private String taskStatus;
    
	private MQTTConnection connectionRobot;

	private final String TOPIC_OUT =  "dataFromUser/lowBattery";

    
/**
 * @name: Constructor for Robot Class
 * @param batteryLevel: battery level of robot
 * @param name: Name of user
 * @param connectionIn: mqtt client
 */
	public Robot(double batteryLevel, String name, MQTTConnection connectionIn)
	{
		this.username = name;
		this.batteryLevel = batteryLevel;
		this.taskStatus = "Sleep";
		this.connectionRobot = connectionIn;
	}
	
/**
 * @name setUserName: Set user name
 * @param Name: username 
 * @return : None
 */   
	public void setUserName(String Name)
	{
		username = Name;
	}
	
/**
 * @name getUserName: get user name
 * @param None 
 * @return : String: username
 */ 
	public String getUserName()
	{
		return username;
	}
	
/**
 * @name getBatteryLevel:  get battery level
 * @param : None 
 * @return : String: username
 */ 
	public double getBatteryLevel()
	{
		return batteryLevel;
	}
	
/**
 * @name useBattery:  reduce battery level
 * @param : None 
 * @return : None
 */ 
	public void useBattery()
	{
		if(batteryLevel > BATTERY_LEVEL_TO_RECHARGE)
		{
			batteryLevel -= 0.1;
		}
		
	}
	
/**
 * @name useBattery:  reduce battery level
 * @param : None 
 * @return : None
 */ 	
	public String CurrentTaskStatus()
	{
		return taskStatus;
	}
	
/**
 * @name setTaskStatus: set the current task status of robot
 * @param : String: request 
 * @return : None
 */ 
	public void setTaskStatus(String UserReq)
	{
		taskStatus = UserReq;
	}
	
/**
 * @name chargeBattery: charge battery to 100%
 * @param : String: request 
 * @return : None
 */ 		
	public void chargeBattery()
	{
		/*increment battery level by 10% until battery level reaches 100%*/
		while(batteryLevel < BATTERY_LEVEL_TO_START_CLEANING )
		{
			this.connectionRobot.PublishMsg(TOPIC_OUT, "LowBatteryTrue");
			batteryLevel = batteryLevel + 10.0;
			/*Rounding off battery level to nearest value to display/notify to user*/
			double charge = Math.round(batteryLevel);  
			System.out.printf("CHARGING UP AT DOCKING STATION  %3.1f", charge);
			System.out.println(" % Charged");
			/*thread delay of 250ms to publish msg*/
			 try {
			 	Thread.sleep(250);
			 }
			 catch(InterruptedException e) {}
		}
		
		/*publish message that battery is fully charged*/
		this.connectionRobot.PublishMsg(TOPIC_OUT, "LowBatteryFalse");

		System.out.println("BATTERY FULLY CHARGED");
		System.out.println("-------RESUME CLEANING --------");
	}

/**
 * @name createWorkarea: create a map of working area.
 * @param : None
 * @return : 2D array of map
 */ 
    public char[][] createWorkarea()
    {
    	char [][] area = new char[20][50];
    	/*Array of static obstacles in a room to be cleaned*/
    	final int[][] OBSTACLES = {   /*x1,y1,x2,y2 coordinates of obstacles assumed*/                        
								      {3,   6, 10, 17},       
								      {10, 40, 15, 47},    
								      {00, 45, 02, 50},
								      {12,  6, 14, 27}
								  };
    	
    	
    	for(int x = 0; x < area.length; x++) 
		{
			for(int y = 0; y < area[0].length; y++) 
			{				
				area[x][y]='X';
			}
		}
		
		for(int i = 0; i<OBSTACLES.length; i++)
		{
			for( int x = OBSTACLES[i][0]; x < OBSTACLES[i][2]; x++) 
			{
				for( int y = OBSTACLES[i][1]; y < OBSTACLES[i][3]; y++) 
				{
					/*area not to be cleaned which is covered by static obstacles*/
					area[x][y]=' ';                    
				}
			}
		}
		
    	return area;
    }
    
/**
 * @name createWorkarea: create a map of working area.
 * @param : None
 * @return : 2D array of map
 */ 
    public void show(char [][] area)
	{
		System.out.println("Mapped area: ");
		for(int x = 0; x < area.length; x++) 
		{
			for(int y = 0; y < area[0].length; y++) 
			{
				System.out.print(area[x][y]);
			}
			System.out.println();
		}
		/*set task status to mapping done after a virtual map of area to be cleaned is ready*/
		setTaskStatus("Mapping Done");
	}
    
/**
 * @name cleanedArea: create a map of area cleaned.
 * @param : x: int x coordinate of area cleaned
 * @param : y: int y coordinate of area cleaned
 * @return : 2D array of map
 */ 
	public char[][] cleanedArea(int x, int y, char [][] area)
	{	
		String areaWithoutstaticObs = "" + area[x][y];
		if(areaWithoutstaticObs.equals("X"))
		{
			/*replace the positions of cleaned area in the working area  X map with . */
			area[x][y] = '.';   
		}

    	return area;
	}
}
	
	
	
	
	
	
	
	
	
	
	

