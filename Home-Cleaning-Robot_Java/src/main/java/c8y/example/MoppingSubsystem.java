package c8y.example;

/**
 * @author Diksha Rani, Simon Welling, Anjali Singh, Jesus Lorenzo Lucas
 * @name MoppingSubsystem : Perfoms  mopping based on user input, monitor water level and battery level 
 *
 */
public class MoppingSubsystem extends Robot{
	
	private double waterLevel;
	public final double MAX_WATER_LEVEL_IN_BIN = 100.0;
	public final double MIN_WATER_LEVEL_IN_BIN = 10.0;
	public MoppingSubsystem(double batteryLevel, String name, MQTTConnection connectionIn) 
	{
		super(batteryLevel, name, connectionIn);
		this.waterLevel =  0.0;
	}
/**
* @name : checkWaterLevel() : Checks the water level and notify user if water level is lesser than 10%.
* @param : void
* @return : boolean
*
*/

	public boolean checkWaterLevel()
	{
		boolean Iswaterless = false;
		if(waterLevel < MIN_WATER_LEVEL_IN_BIN)
		{
			Iswaterless = true;
		}
		return Iswaterless;
	}
	
/**
* @name   : fillWaterBin() : Refill water and notify user once water is filled
* @param  : void
* @return : void
*
*/	
	public void fillWaterBin()
	{
		while(waterLevel < MAX_WATER_LEVEL_IN_BIN)
		{
			waterLevel = waterLevel + 10.0;
			System.out.println("FILLING WATER BIN");
		}
		System.out.println("WATER BIN FULL");
	}
	
/**
* @name   : getWaterLevel() : Notify the current water level in bin
* @param  : void
* @return : double
*
*/	
	public double getWaterLevel()
	{
		return waterLevel;
	}

/**
* @name   : useWater() : Notify the reamaining water level in bin after rinsing
* @param  : void
* @return : void
*
*/

	public void useWater()
	{
		if(waterLevel>10)
		{
			waterLevel = waterLevel - 10;
		}
		
	}
/**
* @name   : run() : Notify user the ongoing status of mopping while monitoring water level, battery level
* @param  : void
* @return : void
*
*/	
	public void run()
	{
		char [][] area = new char[20][50];
		final int RINISING_TIME_INTERVAL = 8;
    	area = createWorkarea();
    	setTaskStatus("CLeaning Now");
    	for( int i = 0; i < area.length ; i++)
		{
			for(int j = 0; j < area[0].length ; j++)
			{
				/*If battery level is lesser than 10% notify user Low Battery */
				if(getBatteryLevel() <= BATTERY_LEVEL_TO_RECHARGE )
				{
					System.out.println("LOW BATTERY");
					chargeBattery();
				}
				
				/*If water level is lesser than 10% notify user Low Water Level */
				if(checkWaterLevel())
				{
					System.out.println("------NOTIFY USER-------");
					fillWaterBin();
					System.out.println("------RESUME MOPPING-------");
				}
				
				/*Rinse the MOP perodically*/
				if(0 == (i% RINISING_TIME_INTERVAL))
				{
					System.out.println("---------RINSING---------");
					useWater();
				}
				try 
				{
					Thread.sleep(250);
				}
				catch(InterruptedException e) {}						
				System.out.printf("(%d,%d) Area cleaned \n" , i , j);
				char [][] moppedArea = cleanedArea(i,j, area);
				show(moppedArea);					
				useBattery();
			}					
						
		}
    	setTaskStatus("Mopping Done");
						
	}

}
