/**
 * Created By : Anjali Singh, Jesus Lorenzo Lucas, Diksha Rani, Simon Welling
 * Date of Creation : 01.02.2021
 */

package c8y.example;

import org.eclipse.paho.client.mqttv3.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 * MQTTConnection class creates a connection to the MQTT broker
 * 
 * Extended from MQTTClient class
 */
public class MQTTConnection extends MqttClient
{
    String serverUrl;
    String clientId;
    MqttClientPersistence persistence; 
    boolean connectionStatus;
    String lastPayload;

    /** 
     * Constructor that initialice a client of MqttClient with server URL, client ID and presistence
     *  
     * @param serverUrl_In : Url of broker
     * @param clientId_In : Id of the client in the MQTT connection
     * @param persistence_In : Persistence of MQTT client
     * @throws MqttException
    */
	public MQTTConnection(String serverUrl_In, String clientId_In, MqttClientPersistence persistence_In) throws MqttException 
    {		
		super(serverUrl_In, clientId_In, persistence_In);	
        connectionStatus = false;
        lastPayload = "";      
    }

    /**
     * Connect to the server if the connection is not established
     */
    public void connectToServer()
    {
        if (!connectionStatus)
        {
            try{
                super.connect();  
                connectionStatus = true;
            } catch (Exception e)
            {
                connectionStatus = false;
                System.out.println("Error in connectToServer!");
                e.printStackTrace();
            }
        }
    }

    /**
     * Publish a message in string format in the desired topic
     * 
     * @param topic : Topic to publish the message
     * @param msgToPublish : Message to be published in the topic
     */

    public void PublishMsg(String topic, String msgToPublish)
    {
        try{
            super.publish(topic, msgToPublish.getBytes(), 2, false);
        } catch (Exception e)
        {
            connectionStatus = false;
            System.out.println("Error in PublishMsg!");
            e.printStackTrace();
        }
    }

    /**
     * Creates a file if it does not exist and subscribe to the desired topic
     * 
     * @param topic : Topic to subscribe for receiving messages
     * @param fileName : Name of the file to store data
     */
    public void subscribeTopic(String topic, String fileName)
    {
        try{
        	File FileSensor = new File(fileName);
            if (FileSensor.createNewFile()) 
            {
                System.out.println("File created: " + FileSensor.getName());
            } 
            super.subscribe(topic, new IMqttMessageListener() {
                /* A callback function is also defined to handle the messages received from the topics
                *  - If the topic is related to the sensorData, the payload is written in a file
                *  - If the message is related to another topic, it is shown to the user*/
                public void messageArrived (final String topic, final MqttMessage message) throws Exception {
                    final String payload = new String(message.getPayload());
                    
                    if (topic.equals("dataFromRover/sensorData"))
                    {
                        try {
                            FileWriter myWriter = new FileWriter(fileName, true);
                            myWriter.append(payload + "\n");
                            myWriter.close();
                        } catch (IOException e) {
                            System.out.println("Error!");
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        System.out.println("Topic: " + topic + " / Message: " + payload);
                        lastPayload = payload;
                        
                    }

                }
            });
        } catch (Exception e)
        {
            connectionStatus = false;
            System.out.println("Error in subscribeTopic!");
            e.printStackTrace();
        }
    }

    /**
     * Get method to access the lastPayload value
     * 
     * @return last payload received in subscriber callback function
     */
    public String getPayload()
    {
        return lastPayload;
    }

    /**
     * Reset method to assign an empty string to lastPayload
     */
    public void resetPayload()
    {
        lastPayload = "";
    }

    /**
     * Get method to check the connection status
     * 
     * @return current status of the connection
     */
    public boolean checkConnectionStatus()
    {
        return connectionStatus;
    }

}
