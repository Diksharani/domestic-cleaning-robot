package c8y.example;

import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.Scanner;

/**
 * Main application to invoke functionalities of robot.
 *
 * @author Diksha Rani, Simon Welling, Anjali Singh, Jesus Lorenzo Lucas
 * @version 02/08/2021
 */
public class App
{

	public static void main(String[] args) throws MqttException 
	{	    

		/* MQTT Connection*/
		final String TOPIC_IN =  "dataFromRover/sensorData";
		final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

		MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
		connectionRobot.connectToServer();
		connectionRobot.subscribeTopic(TOPIC_IN, "SensorData.txt");

		String task = "UserInput";
		Scanner userinput = new Scanner(System.in);
		Robot robot = new Robot(100.0, "Diksha", connectionRobot);

		System.out.println("Enter the cleaning mode: "  );
		task = userinput.next();
		
		while(true)
		{			
			switch (task)
			{
				case "mopping":		
					MoppingSubsystem mop = new MoppingSubsystem(100.0, "Diksha", connectionRobot);
					if(mop.getBatteryLevel() >= mop.BATTERY_LEVEL_TO_START_CLEANING)
					{
						mop.run();
					}
					else 
					{
						System.out.println("------NOTIFY USER TO CHARGE BATTERY------");
					}

					if(mop.CurrentTaskStatus().equals("Mopping Done"))
					{						
						System.out.println("--------MOPPING DONE-----------");						
					}
					task = "UserInput";
					break;
					
				case "vacuum":		
					VacuumSubsystem vac = new VacuumSubsystem(100.0, "Diksha", connectionRobot);
					if(vac.getBatteryLevel() >= vac.BATTERY_LEVEL_TO_START_CLEANING)
					{
						vac.run();
					}
					else 
					{
						System.out.println("------NOTIFY USER TO CHARGE BATTERY------");
					}

					if(vac.CurrentTaskStatus().equals("Vacuum cleaning Done"))
					{
						
						System.out.println("-----------VACUUM CLEANING DONE------------");						
					}
					
					task = "UserInput";
					break;
					
				case "mapping":	
					char [][] area = new char[20][50];
					if(robot.getBatteryLevel() >= robot.BATTERY_LEVEL_TO_START_CLEANING)
					{
						area = robot.createWorkarea();						
						robot.show(area);
					}
					else
					{
						System.out.println("------NOTIFY USER TO CHARGE BATTERY------");
					}
					
					if(robot.CurrentTaskStatus().equals("Mapping Done"))
					{						
						System.out.println("-----------WORKING AREA MAP CREATED------------");						
					}
					
					task = "UserInput";
					break;
				
				case "UserInput":	
				    System.out.println("Enter the cleaning mode: "  );
				    task = userinput.next();
					break;
									
				default:
					System.out.println("Enter a valid cleaning mode: "  );
					task = userinput.next();
					break;					
			}
		}	
	}
}
