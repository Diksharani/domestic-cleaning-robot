package c8y.example;

/**
 * Class for vacuum cleaning subsystem of Home cleaning robot.
 *
 * @author Diksha Rani, Simon Welling, Anjali Singh, Jesus Lorenzo Lucas
 * @version 02/08/2021
 */

public class VacuumSubsystem extends Robot
{
	private double dirtlevel;
	public final double MAX_DIRT_LEVEL = 95.0;
	
/**@name VacuumSubsystem: Constructor for vacuum subsystem
 * @param dirtlevel: dirt level of the Robots bin
 * @param MAX_DIRT_LEVEL: Level of dirt the robot empties the bin 
 * @param batteryLevel: battery level of robot super class
 * @param name: Name of user from robot super class
 * @param connectionIn: mqtt client from robot super class
 */
	public VacuumSubsystem(double batteryLevel, String name, MQTTConnection connectionIn)
	{
		super(batteryLevel, name, connectionIn);
		this.dirtlevel  = 0.0;
	}
	
	/**
	* @name run(): Runs the mopping functionality
	* @param None
	* @return None
	*/
	public void run()
	{
		char [][] area = new char[20][50];
    	area = createWorkarea();
    	setTaskStatus("CLeaning Now");
		for( int i = 0; i < area.length ; i++)
		{
			for(int j = 0; j < area[0].length ; j++)
			{
				/* recharge when BatteryLevel is low*/
				if(getBatteryLevel() <= BATTERY_LEVEL_TO_RECHARGE )
				{
					System.out.println("LOW BATTERY");
					chargeBattery();
				}
				/* empty bin when full*/
				if(checkDirtLevel())
				{
					System.out.println("------EMPTY BIN-------");
					dirtlevel = 0.0;
					System.out.println("------RESUME VACUUM CLEANING-------");
				}
				/*thread delay*/
				try 
				{
					Thread.sleep(250);
				}
				catch(InterruptedException e) {}
				System.out.printf("(%d,%d) Area cleaned \n" , i , j);
				char [][] vacuumedArea = cleanedArea(i,j, area);	// mark the area cleaned
				show(vacuumedArea);
				/*increase dirt level in robot*/
				SetDirtLevel();
				/*consume battery*/
				useBattery();
			}						
		}
		/*set current task status to Done*/
		setTaskStatus("Vacuum cleaning Done");			
	}
	
	/**
	* @name checkDirtLevel(): checks whether the dirtlevel reached maximum
	* @param void
	* @return :boolean Returns true if maximum dirt level is reached
	*/
	public boolean checkDirtLevel()
	{
		boolean dirtLimitReached = false;
		if(this.dirtlevel >= MAX_DIRT_LEVEL)
		{
			dirtLimitReached = true;
		}
		return dirtLimitReached;
	}
	
	/**
	* @name SetDirtLevel(): increases the dirtlevel 
	* @param  : void
	* @return : void
	*/
	public void SetDirtLevel()
	{
		dirtlevel = dirtlevel + 10;		
	}
}

