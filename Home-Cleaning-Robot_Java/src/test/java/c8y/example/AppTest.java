package c8y.example;

/**
 * @author Diksha Rani
 *
 */

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.FileReader;
import java.io.File;
import java.util.Scanner;

import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Unit test for App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }


///********** MQTT CONNECTION TEST***************/

    /**
     * @Test for check mqtt connection
     * @throws Exception 
     */
    public void testConnection() throws MqttException
    {
        final String TOPIC_OUT =  "dataFromUser/test";
		final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";
        final String fileStr = "Test.txt";

        boolean result = false;
        
        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();
            
        connectionRobot.PublishMsg(TOPIC_OUT, "Test");

        System.out.println("\n-------Connection Status: " + connectionRobot.checkConnectionStatus());

        assertEquals( true, connectionRobot.checkConnectionStatus() );
    }
    
    /**
     * @Test for check mqtt connection: failed case
     * @throws Exception 
     */

    public void testConnectionFail() throws MqttException
    {
        final String TOPIC_OUT =  "dataFromUser/test";
		final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";
        final String fileStr = "Test.txt";

        boolean result = false;
        
        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        
        System.out.println("\n-------Connection Status: " + connectionRobot.checkConnectionStatus());

        connectionRobot.PublishMsg(TOPIC_OUT, "Test");

        assertEquals( false, connectionRobot.checkConnectionStatus() );
    }


/********** CONDITIONS TEST***************/

    /**
     * @Test for use of Battery
     * @throws MqttException 
     */
    public void testCheckUseBattery() throws MqttException
    {
        final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        // Test Battery Check
        Robot robot = new Robot(100.0, "Diksha", connectionRobot);	
        for(int i=0; i<=10; i++)
        {
        	robot.useBattery();
        }
               
        assertEquals(99, Math.round(robot.getBatteryLevel()));
    }

    /**
     * @Test for charge Battery : boundary condition
     * @throws MqttException 
     */
    public void testCheckChargeBattery() throws MqttException
    {
        final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        // Test Battery Check
        Robot robot = new Robot(100.0, "Diksha", connectionRobot);	
        
        robot.chargeBattery();
        assertEquals(100, Math.round(robot.getBatteryLevel()));
    }
    
    /**
     * @Test for Charge Battery
     * @throws MqttException 
     */
    public void testCheckChargeBatteryNotfull() throws MqttException
    {
        final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        // Test Battery Check
        Robot robot = new Robot(10.0, "Diksha", connectionRobot);	
        
        assertEquals(10, Math.round(robot.getBatteryLevel()));
    }

    /**
     * @Test for check dirt level: boundary condition
     * @throws MqttException 
     */
    public void testCheckDirtLevel() throws MqttException
    {
        final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

		VacuumSubsystem vac = new VacuumSubsystem(100.0, "Diksha", connectionRobot);
	    for( double i = 0.0; i < vac.MAX_DIRT_LEVEL; i = i + 10.0 )		
	    {
	    	vac.SetDirtLevel();	    	
	    }
	    assertEquals(true, vac.checkDirtLevel());
    }
    
    /**
     * @Test for check dirt level
     * @throws MqttException 
     */
    public void testCheckDirtLevelFalse() throws MqttException
    {
        final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

		VacuumSubsystem vac = new VacuumSubsystem(100.0, "Diksha", connectionRobot);
	    vac.SetDirtLevel();	
	    vac.SetDirtLevel();
	    assertEquals(false, vac.checkDirtLevel());
    }

    /**
     * Test for check use of water: boundary condition
     * @throws MqttException 
     */
    public void testCheckUseWater() throws MqttException
    {

        final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        // Test Water Level Check
        MoppingSubsystem mop = new MoppingSubsystem(30.0, "Diksha", connectionRobot);
        mop.useWater();
        assertEquals(0.0, mop.getWaterLevel());	

    }

    /**
     * @Test for fill water
     * @throws MqttException 
     */
    public void testCheckFillWater() throws MqttException
    {

        final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        // Test Water Level Check
        MoppingSubsystem mop = new MoppingSubsystem(10.0, "Diksha", connectionRobot);
        mop.useWater();
        mop.useWater();
        mop.fillWaterBin();
        assertEquals(mop.MAX_WATER_LEVEL_IN_BIN, mop.getWaterLevel());

    }


/********** MODE TESTS***************/

    /**
     * @Test for check vacuum mode
     * @throws MqttException 
     */
    public void testCheckVacuum() throws MqttException
    {
		final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        VacuumSubsystem vac = new VacuumSubsystem(100, "Diksha", connectionRobot);			
        vac.run();

        assertEquals("Vacuum cleaning Done", vac.CurrentTaskStatus());
    }

    /**
     * Test for check mopping mode
     * @throws MqttException
     */
    public void testCheckMopping() throws MqttException
    {

		final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        MoppingSubsystem mop = new MoppingSubsystem(100, "Diksha", connectionRobot);						
        mop.run();

        assertEquals("Mopping Done", mop.CurrentTaskStatus());
        
    }
                            
    /**
     * Test for check mapping mode
     * @throws MqttException
     */
    public void testCheckMapping() throws MqttException
    {

		final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        Robot robot = new Robot(100, "Diksha", connectionRobot);						
        char [][] area = robot.createWorkarea();
        robot.show(area);

        assertEquals("Mapping Done", robot.CurrentTaskStatus());
        
    }
    /**
     * Test for check default mode
     * @throws MqttException
     */
    public void testCheckDefaultMode() throws MqttException
    {

		final String URL_MQTT = "tcp://broker.emqx.io";
		final String ClientID = "javaClient";

        MQTTConnection connectionRobot = new MQTTConnection(URL_MQTT, ClientID, null);
        connectionRobot.connectToServer();

        Robot robot = new Robot(100, "Diksha", connectionRobot);						

        assertEquals("Sleep", robot.CurrentTaskStatus());
        
    }
}
